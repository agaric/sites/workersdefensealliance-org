# Twin Cities Workers Defence Alliance

The workersdefensealliance.org web site hosted by Drutopia.

## Development

This project uses [ddev](https://www.ddev.com/) to provide web and database servers for this project.

```
ddev start
```

### Syncing db/file content

Use `drush` for database synchronization for testing. For example, to copy live locally:

```
drush sql-sync @live @self
```

## Run tests

We are developing "In Other Words" for Drutopia usage on this site

```bash
ddev ssh
./vendor/bin/phpunit --testsuite=inotherwords
```

## Deployment

TC Worker Defence Alliance is currently using a Platform as a Service version of Drutopia.

Set up [drutopia_host](https://gitlab.com/drutopia-platform/drutopia_host) and [hosting_private](https://gitlab.com/drutopia-platform/hosting_private), as documented in hosting private.

Then use [ahoy](https://github.com/ahoy-cli/ahoy/), from within the hosting_private directory.

Ensure all three related repositories are up-to-date with:

```
ahoy git-pull-all
```

If Drutopia's PaaS base has changed (the composer.lock in this directory), produce and push a new build. Note, this is the base distribution used for all generic Drutopia builds:

```
ahoy deploy-build next
```

To deploy everything else (config, templates, styles):

```
ahoy deploy-site tc_worker_live
```

Then record the deployment. This applies to both deploy-site and deploy-build record keeping:
Navigate to `/drutopia_host/build_artifacts`
Add all new files generated with `git add . `
Commit the files with a message on what you deployed with `git commit -m "Add artifacts"`
Push the changes to the repo with `git push`

If you need to overwrite live configuration (only after confirming you've committed any parts you want to keep) you can use ahoy for that too with `deploy-site-force`.

### Drupal settings file management

The Drupal settings files are also managed by Drutopia. On each deploy, the file is generated and replaces the `settings.local.php` that is on the server with whatever settings will match the Drutopia configuration. Therefore, in order to add settings, you must edit the vault settings using:

```
ahoy vault-edit
```

Look for the Yaml section for tc_worker_live and edit the php_settings_code section to add items to that site's setting overrides. For example:

```
      ...
      online: True
      php_settings_code: |2
        $settings['super_secret'] = p@ssw0rd;
        $config['life.answer'] = 42;
      server_aliases:
        - agaric.com
      ...
```
